//#include "SoftwareSerial.h"
#include "PinChangeInterrupt.h"
#include "Encoder.h"

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define RESET_PB 8
#define READ_PB 9

#define CLK 3
#define DT 2

#define BL_RX 5
#define BL_TX 6

#define TICKS_FACTOR 3.00//no of ticks per 1cm

void resetCount(void);
void readCount(void);
void displayInit(void);
void displayDistance(float distance);

volatile bool enable_read_ = false;
volatile bool set_zero_ = false;
long zero_position_ = 0;
float distance_ = 0;
float pre_distance_ = 0;

Encoder rotary_enc(CLK, DT);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


void setup() {

  Serial.begin(9600);

  displayInit();

  pinMode(RESET_PB, INPUT_PULLUP);
  pinMode(READ_PB, INPUT_PULLUP);
  attachPCINT(digitalPinToPCINT(RESET_PB), resetCount, FALLING);
  attachPCINT(digitalPinToPCINT(READ_PB), readCount, FALLING);

}


void loop() {

  distance_ = (rotary_enc.read() - zero_position_) / TICKS_FACTOR;
  //Serial.println(distance_,2);
  //delay(500);
  if (pre_distance_ != distance_)
  {
    Serial.println(distance_, 2);
    if (distance_ < 0.00)
    {
      display.clearDisplay();
      display.setTextSize(1);
      display.setCursor(10, 10);
      display.print("Please");
      display.setTextSize(2);
      display.setCursor(20, 45);
      display.println("CALIBRATE");
      display.display();
    }
    else
    {
      displayDistance(distance_);
    }
    pre_distance_ = distance_;
  }

  if (set_zero_)
  {
    zero_position_ = rotary_enc.read();
    //Serial.print("Zero posititon set to: ");
    Serial.println(0.00);

    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(10, 10);
    display.println("Measured Distance");
    display.setTextSize(2);
    display.setCursor(20, 45);
    display.println("RESET !!!");
    display.display();

    set_zero_ = false;
  }

  /*if (enable_read_)
    {
    distance_ = (rotary_enc.read() - zero_position_) / TICKS_FACTOR;
    Serial.println(distance_);
    displayDistance(distance_);
    enable_read_ = false;
    }*/
}

void displayDistance(float distance)
{
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(10, 10);
  display.println("Measured Distance= ");
  display.setTextSize(2);
  display.setCursor(0, 45);
  display.print(distance);
  display.setTextSize(1);
  display.setCursor(100, 55);
  display.println("cm");
  display.display();
}
void resetCount(void)
{
  set_zero_ = true;
}

void readCount(void)
{
  enable_read_ = true;
}

void displayInit(void)
{
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  {
    Serial.println(F("SSD1306 allocation failed"));
    return;
  }

  delay(1000);
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(30, 10);
  display.println("Rubber Milk");
  display.setCursor(15, 40);

  display.println("Distance Measuring");
  display.display();
}
